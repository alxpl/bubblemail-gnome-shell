# Run meson/post-install.sh (glib-compile-schemas)
option(
  'post_install',
  type: 'boolean',
  value: false,
  description: 'Run meson/post-install.sh'
)

# GNOME Shell LIBDIR
option(
  'gnome_shell_libdir',
  type: 'string',
  value: '',
  description: 'LIBDIR for GNOME Shell'
)

# GSettings schema directory
option(
  'gsettings_schemadir',
  type: 'string',
  value: '',
  description: 'GSettings Schema directory'
)
