/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2023 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GObject = imports.gi.GObject;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Opts = Me.imports.opts;
const Gettext = imports.gettext.domain(Opts.GETTEXT_DOMAIN);
const _ = Gettext.gettext;
const shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);

function widget_append(widget, child) {
  shellVersion < 40 ? widget.add(child) : widget.append(child);
}

function set_widget_child(widget, child) {
  shellVersion < 40 ? widget.add(child) : widget.set_child(child);
}

function labeled_box(text) {
  let hbox = new Gtk.Box({orientation: Gtk.Orientation.HORIZONTAL, spacing: 10});
  widget_append(hbox, new Gtk.Label({label: text, xalign: 0, hexpand: true}));
  return hbox;
}

function switch_option(settings, key, text) {
  let hbox = labeled_box(text);
  let switcher = new Gtk.Switch({active: settings.get_boolean(key), halign: Gtk.Align.END});
  settings.bind(key, switcher, 'active', Gio.SettingsBindFlags.DEFAULT);
  widget_append(hbox, switcher);
  return hbox;
}

function init() {  // eslint-disable-line no-unused-vars
  ExtensionUtils.initTranslations();
}

var BubblemailSettingsWidget = GObject.registerClass(
  class BubblemailSettingsWidget extends Gtk.Box {
    _init() {
      super._init({orientation: Gtk.Orientation.VERTICAL, spacing: 16,
                   margin_top: 16, margin_bottom: 16, margin_start: 16, margin_end: 16});
      let settings = ExtensionUtils.getSettings();
      let general_frame = new Gtk.Frame({ margin_top: 10, margin_bottom: 10});
      general_frame.set_label(_('General'));
      let general_vbox = new Gtk.Box(
        {orientation: Gtk.Orientation.VERTICAL, spacing: 6,
         margin_top: 10, margin_bottom: 10, margin_start: 10, margin_end: 10});
      widget_append(general_vbox, switch_option(
        settings, Opts.KEY.ALWAYS_SHOW, _('Show the indicator when maillist is empty')));
      widget_append(general_vbox, switch_option(
        settings, Opts.KEY.ANIMATE_COUNTER, _('Animate mail count label')));
      widget_append(general_vbox, switch_option(settings, Opts.KEY.COUNTER_OVER_ICON,
        _('Show mail counter over the mail icon')));
      widget_append(general_vbox, switch_option(settings, Opts.KEY.SHOW_MENU_ICONS,
        _('Show menu icons')));
      widget_append(general_vbox, switch_option(settings, Opts.KEY.SHOW_MENU_MAILAPP,
        _('Show mail application launcher')));
      set_widget_child(general_frame, general_vbox);
      widget_append(this, general_frame);

      let maillist_frame = new Gtk.Frame({});
      maillist_frame.set_label(_('Mail list'));
      let maillist_vbox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL, spacing: 6,
          margin_top: 10, margin_bottom: 10, margin_start: 10, margin_end: 10});
      widget_append(maillist_vbox, switch_option(settings, Opts.KEY.NEWEST_FIRST,
        _('Show newest mails at first')));
      let vbox = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL, spacing: 6});
      let hbox = labeled_box(_('Hide mail subjects when mail list exceeds'));
      let spinbutton = Gtk.SpinButton.new_with_range(0, 50, 1);
      spinbutton.set_value(settings.get_int(Opts.KEY.MIN_HIDE_SUBJECT));
      settings.bind(Opts.KEY.MIN_HIDE_SUBJECT, spinbutton, 'value', Gio.SettingsBindFlags.DEFAULT);
      widget_append(hbox, spinbutton);
      widget_append(vbox, hbox);
      widget_append(vbox, new Gtk.Label(
        {label: '<i>' +  _('Subjects will be always visible if set to 0') + '</i>', xalign: 1,
        use_markup: true}));
      widget_append(maillist_vbox, vbox);
      widget_append(maillist_vbox, switch_option(settings, Opts.KEY.GROUP_BY_ACCOUNT,
        _('Group mails by account')));
      widget_append(maillist_vbox, switch_option(settings, Opts.KEY.SHOW_AVATARS,
        _('Show avatars')));
      widget_append(maillist_vbox, switch_option(settings, Opts.KEY.SHOW_DATES, _('Show dates')));
      set_widget_child(maillist_frame, maillist_vbox);
      widget_append(this, maillist_frame);
    }
  }
);

function buildPrefsWidget() {  // eslint-disable-line no-unused-vars
  let widget = new BubblemailSettingsWidget();
  shellVersion < 40 ? widget.show_all() : widget.show();
  return widget;
}
