/* Bubblemail - GNOME-Shell extension frontend
*
* Copyright 2019 - 2023 Razer <razerraz@free.fr>
* Copyright 2013 - 2019 Patrick Ulbrich <zulu99@gmx.net>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
* MA 02110-1301, USA.
*/

const {Clutter, St, GObject} = imports.gi;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Shell = imports.gi.Shell;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const ExtensionUtils = imports.misc.extensionUtils;
const Config = imports.misc.config;
const Me = ExtensionUtils.getCurrentExtension();
const shellVersion = parseFloat(imports.misc.config.PACKAGE_VERSION);
const Opts = Me.imports.opts;
const Utils = Me.imports.utils;
const MailItem = Me.imports.mailitem;
const Gettext = imports.gettext.domain(Opts.GETTEXT_DOMAIN);
const _ = Gettext.gettext;

const SERVICE_MIN_VERSION = 1.2;
const NM_STATE_DISCONNECTED = 20;
const INDICATOR_ICON  = 'mail-unread-symbolic';
const ERROR_NOSERVICE = 1;
const ERROR_SERVICE_STOPPED = 2;
const ERROR_VERSION_MISMATCH = 3;
const ERROR_ACCOUNT = 4;

var MailSubMenu = class extends PopupMenu.PopupSubMenu {
  constructor(actor, arrow) {
    super(actor, arrow);
    this.bypass_close = false;
    this.actor.remove_style_class_name('popup-sub-menu');
  };
  close() {
    // Prevent mail menu to be closed overwriting the close method
    if (this.bypass_close) return;
    PopupMenu.PopupSubMenu.prototype.close.call(this);
  };
};


var MailSubMenuItem = GObject.registerClass(  // eslint-disable-line no-unused-vars
class MailSubMenuItem extends PopupMenu.PopupSubMenuMenuItem {

  _init() {
    super._init('', false);
    this.remove_style_class_name('popup-submenu-menu-item');
    this.remove_style_class_name('popup-menu-item');
    Utils.hide_ornament(this.actor);
    this._triangle.hide();
    this.menu.destroy();
    this.menu = new MailSubMenu(this, this._triangle);
    this.menu.connect('open-state-changed', this._subMenuOpenStateChanged.bind(this));
  }
});


var BubblemailIndicator = GObject.registerClass(  // eslint-disable-line no-unused-vars
class BubblemailIndicator extends PanelMenu.Button {

  _init(options, extension) {
    super._init(0.0, null, false);
    this.options = options;
    this.extension = extension;
    this.options.max_width = Math.floor(Utils.get_screen_width()/6);
    this.previous_mail_count = 0;
    this.error_map = [
      [],
      [_('Bubblemail service not found !'), _('Go to download page')],
      [_('Bubblemail service is not running !'), _('Launch settings')],
      [_('Bubblemail service version mismatch, please upgrade !'), _('Go to download page')],
      [_('Account error'), _('Launch settings')]
    ];
    let hbox = new St.BoxLayout({style_class: 'panel-status-menu-box'});
    this.icon = new St.Icon({icon_name: INDICATOR_ICON,  style_class: 'system-status-icon'});
    this.icon_bin = new St.Bin({child: this.icon});
    hbox.add(this.icon_bin);
    this.icon_lbl= new St.Label({
      text: '0', x_align: Clutter.ActorAlign.CENTER, x_expand: true, y_expand: true,
      y_align: Clutter.ActorAlign.CENTER});
    this.icon_space = new St.Label({text: '', style_class: 'arrow'});
    if (this.options.counter_over_icon) {
      hbox.add(this.icon_space);
      this.icon_lbl_bin = new St.Bin({
        style_class: 'counter', child: this.icon_lbl,
        layout_manager: new Clutter.BinLayout()});
      this.icon_lbl_bin.connect('style-changed', () => {
        let themeNode = this.icon_lbl_bin.get_theme_node();
        this.icon_lbl_bin.translation_x = themeNode.get_length('-counter-overlap-x');
        this.icon_lbl_bin.translation_y = themeNode.get_length('-counter-overlap-y');
      });
    } else {
      this.icon_lbl_bin = this.icon_lbl;
      hbox.add(this.icon_lbl);
    }
    this.add_actor(hbox);
    if (this.options.counter_over_icon)
      this.add_actor(this.icon_lbl_bin);
    // Status menu item
    this.status_menuitem = null;
    // Mail submenu
    this.mail_submenu = new MailSubMenuItem();
    this.menu.addMenuItem(this.mail_submenu);
    this.mail_separator = new PopupMenu.PopupSeparatorMenuItem();
    this.menu.addMenuItem(this.mail_separator );
    this.mail_separator.hide();
    // Dismiss + refresh items
    this.dismiss_all = new PopupMenu.PopupImageMenuItem(
      _('Dismiss all'), this.options.show_menu_icons ? 'object-select-symbolic' : null);
    this.dismiss_all.label.add_style_class_name('menu-item');
    Utils.hide_ornament(this.dismiss_all);
    this.dismiss_all.connect('activate', () => {
      this.extension.dismiss_all();
      this.update([]);
    });
    this.menu.addMenuItem(this.dismiss_all);
    this.refresh = new PopupMenu.PopupImageMenuItem(
      _('Refresh'), this.options.show_menu_icons ? 'view-refresh-symbolic' : null);
    this.refresh.label.add_style_class_name('menu-item');
    Utils.hide_ornament(this.refresh);
    this.refresh.connect('activate', () => {
      this.extension.refresh();
    });
    this.menu.addMenuItem(this.refresh);
    // Mail app launcher item
    this.run_mailer = null;
    if (this.options.show_menu_mailapp && this.extension.mail_app) {
      this.run_mailer = new PopupMenu.PopupImageMenuItem(
        _('Open mail app'), this.options.show_menu_icons ? INDICATOR_ICON : null);
      Utils.hide_ornament(this.run_mailer);
      this.run_mailer.label.add_style_class_name('menu-item');
      this.run_mailer.connect('activate', () => {
        Utils.run_desktop_app(this.extension.mail_app);
      });
      this.menu.addMenuItem(this.run_mailer);
    }
    // Settings submenu
    this.settings_submenu = new PopupMenu.PopupSubMenuMenuItem(_('Settings'), true);
    Utils.hide_ornament(this.settings_submenu);
    if (this.options.show_menu_icons)
      this.settings_submenu.icon.icon_name = 'org.gnome.Settings-symbolic';
    this.settings_submenu.label.add_style_class_name('menu-item');
    let menu_item = new PopupMenu.PopupMenuItem(_('Service Settings'));
    menu_item.add_style_class_name('submenu-item');
    menu_item.connect('activate', () => {this.run_service_settings();});
    this.settings_submenu.menu.addMenuItem(menu_item);
    menu_item = new PopupMenu.PopupMenuItem(_('Extension Settings'));
    menu_item.add_style_class_name('submenu-item');
    menu_item.connect('activate', () => {
      if (typeof ExtensionUtils.openPrefs === 'function')
        ExtensionUtils.openPrefs();
      else if (Config.PACKAGE_VERSION.startsWith('3.36'))
        Gio.DBus.session.call('org.gnome.Shell.Extensions', '/org/gnome/Shell/Extensions',
                              'org.gnome.Shell.Extensions', 'OpenExtensionPrefs',
                              new GLib.Variant('(ssa{sv})', [Me.uuid, '', {}]), null,
                              Gio.DBusCallFlags.NONE, -1, null);
      else
        Util.spawn(['gnome-shell-extension-prefs', 'bubblemail@razer.framagit.org']);
    });
    this.settings_submenu.menu.addMenuItem(menu_item);
    this.menu.addMenuItem(this.settings_submenu);
    this.update([]);
  }

  animate_counter() {
    if (!this.options.animate_counter) return;
    this.icon_lbl_bin.restore_easing_state();
    this.icon_lbl_bin.set_scale(2,2);
    this.icon_lbl_bin.save_easing_state();
    this.icon_lbl_bin.set_easing_duration(1000);
    this.icon_lbl_bin.set_scale(1,1);
  }

  _onOpenStateChanged(menu, open) {
    super._onOpenStateChanged(menu, open);
    // Trigger mail menu opening here to adapt it's size correctly in a scrollbox
    this.mail_submenu.setSubmenuShown(true);
    if (this.status_menuitem)  this.status_menuitem.menu.open(false);
  }

  vfunc_allocate(box, flags) {
    if (shellVersion >= 3.38)
      super.vfunc_allocate(box);
    else
      super.vfunc_allocate(box, flags);
    if (!this.options.counter_over_icon) return;
    // get the allocation box of the indicator icon
    let icon_box = this.icon_bin.child.get_allocation_box();
    // create a temporary box for calculating the counter allocation
    let child_box = new Clutter.ActorBox();
    let [unused_w, unused_h, natural_w, natural_h] = this.icon_lbl_bin.get_preferred_size();
    let direction = this.get_text_direction();
    if (direction == Clutter.TextDirection.LTR) {
      // allocate on the right in LTR
      child_box.x1 = icon_box.x2 - natural_w / 2 ;
      child_box.x2 = child_box.x1 + natural_w;
    } else {
      // allocate on the left in RTL
      child_box.x1 = icon_box.x1 - natural_w / 2;
      child_box.x2 = child_box.x1 + natural_w;
    }
    child_box.y1 = icon_box.y2 - natural_h / 2;
    child_box.y2 = child_box.y1 + natural_h;
    if (shellVersion >= 3.38)
      this.icon_lbl_bin.allocate(child_box);
    else
      this.icon_lbl_bin.allocate(child_box, flags);
  }

  populate_warning() {
    if (!Object.prototype.hasOwnProperty.call(this.extension.core_config, 'poll_interval'))
      return;
    if (this.extension.core_config.poll_interval === '0') {
      this.icon_lbl_bin.visible = true;
      if (this.options.counter_over_icon && !this.icon_lbl_bin.has_style_class_name('warning'))
        this.icon_lbl_bin.add_style_class_name('warning');
      if (!this.extension.mail_map.length || !this.options.counter_over_icon) {
        this.icon_lbl.set_text('||');
        this.animate_counter();
      }
      this.add_status('media-playback-pause-symbolic', _('Auto refresh is disabled'),
                      'no-networks-label warning-lbl');
    } else {
      if (this.icon_lbl_bin.has_style_class_name('warning'))
        this.icon_lbl_bin.remove_style_class_name('warning');
    }
  }

  populate_error(error) {
    this.icon_lbl_bin.visible = true;
    this.icon.opacity = 100;
    if (this.options.counter_over_icon && !this.icon_lbl_bin.has_style_class_name('error'))
      this.icon_lbl_bin.add_style_class_name('error');
    this.icon_lbl.set_text('!');
    this.animate_counter();
    this.add_status('dialog-error-symbolic', error[0], 'error-lbl', error[1]);
  }

  populate(mail_map) {
    if (this.options.counter_over_icon && !this.error
      && this.icon_lbl_bin.has_style_class_name('error'))
      this.icon_lbl_bin.remove_style_class_name('error');
    if (mail_map.length) {
      this.mail_submenu.menu.bypass_close = true;
      if (this.run_mailer) this.run_mailer.hide();
      this.mail_submenu.show();
      this.mail_separator.show();
      this.dismiss_all.show();
      if (this.options.group && mail_map[0]['account'].length) {
        this.add_grouped(mail_map);
      } else {
        this.add(mail_map);
      }
    } else {
      if (this.run_mailer) this.run_mailer.show();
    }
  }

  add(mail_map) {
    for (let mail of mail_map) {
      this.mail_submenu.menu.addMenuItem(
        new MailItem.MailItem(mail, this.options, this.extension));
    }
  }

  add_grouped(mail_map) {
    let grp_mailmap = {};
    for (let mail of mail_map) {
      for (let account of this.extension.account_map) {
        if (mail.account != account.uuid) continue;
        if (!Object.prototype.hasOwnProperty.call(grp_mailmap, account.name))
          grp_mailmap[account.name] = [];
        grp_mailmap[account.name].push(mail);
        break;
      }
    }
    let count = 0;
    for (let account of Object.keys(grp_mailmap)) {
      if (!grp_mailmap[account].length) continue;
      let account_menuitem = new PopupMenu.PopupBaseMenuItem(
        {reactive: false, can_focus: false, activate: false, hover: false,
         style_class: 'account-menuitem'});
      Utils.hide_ornament(account_menuitem);
      let account_label = new St.Label({text: account, x_expand: true,
                                        style_class: 'account-lbl'});
      account_label.clutter_text.single_line_mode= true;
      account_menuitem.add_child(account_label);
      this.mail_submenu.menu.addMenuItem(account_menuitem);
      this.add(grp_mailmap[account]);
      if (count)
        account_label.add_style_class_name('account-space');
      count++;
    }
  }

  run_service_settings() {
    Utils.run_desktop_app(Utils.get_settings_app(this.extension.core_config));
  }

  add_status(icon_name, message, style_class, button_label) {
    if (this.status_menuitem) return;
    this.status_menuitem = new PopupMenu.PopupSubMenuMenuItem(message, true);
    this.status_menuitem.icon.icon_name = icon_name;
    this.status_menuitem.label.add_style_class_name('menu-item');
    this.status_menuitem.add_style_class_name(style_class);
    if (button_label) {
      let item = new PopupMenu.PopupMenuItem(button_label);
      item.add_style_class_name('submenu-item');
      item.connect('activate', () => {
        this.menu.close();
        switch (this.error) {
        case ERROR_NOSERVICE: case ERROR_VERSION_MISMATCH:
          Gio.AppInfo.launch_default_for_uri('http://bubblemail.free.fr/', null);
          break;
        case ERROR_SERVICE_STOPPED:
        case ERROR_ACCOUNT:
          this.run_service_settings();
          break;
        default:
          break;
        }
      });
      this.status_menuitem.menu.addMenuItem(item);
    }
    this.menu.addMenuItem(this.status_menuitem, 0);
  }

  update_icon_label(mail_map) {
    let label = mail_map.length <= 99 ? mail_map.length.toString() : '...';
    if (label != '0')
      this.icon_lbl.set_text(label);
    if (mail_map.length > 0) {
      this.icon_space.show();
      this.icon_lbl_bin.visible = true;
      this.icon.opacity = 255;
      if (this.previous_mail_count < mail_map.length)
        this.animate_counter();
    } else {
      this.icon_space.hide();
      this.icon_lbl_bin.visible = this.error;
      this.icon.opacity = 130;
    }
    this.previous_mail_count = mail_map.length;
    let offline = false;
    for (let status of this.extension.status_map) {
      if (status.error_code.unpack() >= 0) continue;
      offline = true;
      break;
    }
    if (this.extension.nm_state == NM_STATE_DISCONNECTED || offline) {
      if (this.options.counter_over_icon && !this.icon_lbl_bin.has_style_class_name('offline'))
        this.icon_lbl_bin.add_style_class_name('offline');
    } else {
      if (this.options.counter_over_icon && this.icon_lbl_bin.has_style_class_name('offline'))
        this.icon_lbl_bin.remove_style_class_name('offline');
    }
  }

  update(mail_map) {
    if (this.status_menuitem) {
      this.status_menuitem.destroy();
      this.status_menuitem = null;
    };
    this.mail_submenu.menu.bypass_close = false;
    this.mail_submenu.menu.removeAll();
    this.mail_submenu.menu.close();
    this.mail_submenu.hide();
    this.mail_separator.hide();
    this.settings_submenu.hide();
    this.dismiss_all.hide();
    this.refresh.hide();
    if (!this.extension.dbus_map.bubblemail) {
      this.icon_lbl_bin.visible = true;
      this.icon.opacity = 100;
      let settings_app = Utils.get_settings_app(this.extension.core_config);
      if (!settings_app || !Shell.AppSystem.get_default().lookup_app(settings_app)) {
        Utils.ilog('No service found !');
        this.error = ERROR_NOSERVICE;
        this.populate_error(this.error_map[ERROR_NOSERVICE]);
        return;
      }
      this.error = ERROR_SERVICE_STOPPED;
      this.populate_error(this.error_map[ERROR_SERVICE_STOPPED]);
      return;
    }
    if (!Object.prototype.hasOwnProperty.call(this.extension.core_config, 'version')) {
      this.populate_error(this.error_map[ERROR_VERSION_MISMATCH]);
      return;
    }
    let service_version = this.extension.core_config.version.replace('beta', '');
    if (parseFloat(service_version.replace('alpha', '')) < SERVICE_MIN_VERSION) {
      this.populate_error(this.error_map[ERROR_VERSION_MISMATCH]);
      return;
    }
    this.refresh.show();
    this.settings_submenu.show();
    this.error = 0;
    for (let status of this.extension.status_map) {
      if (status.error_code.unpack() <= 0 || status.error_count.unpack() < 3) continue;
      let account;
      for (let known_account of this.extension.account_map) {
        if (known_account.uuid != status.uuid.unpack()) continue;
        account = known_account;
        break;
      }
      if (!account || !account.enabled) {
        continue;
      }
      let account_error = [...this.error_map[ERROR_ACCOUNT]];
      account_error[0] = _('Error on account ');
      account_error[0] += `${account.name}: ${status.error_message.unpack()}`;
      this.error = ERROR_ACCOUNT;
      this.populate_error(account_error);
    }
    this.update_icon_label(mail_map);
    this.populate_warning();
    this.populate(mail_map);
  }
});
